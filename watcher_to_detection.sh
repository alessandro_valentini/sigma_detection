#! /bin/bash

# This script converts an xpack-watcher generated using sigmac into a detection. To achieve this is extracts values from the watcher, providing 
# defaults values if needed (e.g. range or tags which can be missing) and replace them in provided templates: filter.json, range.json, 
# detection_template.json
# Some fields are generated directly from the script such as creation date and uuid

# Usage 
# Input is a xpack-watcher created using sigmac
# Output is in ndjson format (i.e. a single-line json) printed. You can save it simply redirecting output or make is more readable using jq e.g.
# ./watcher_to_detection.sh <WATCHER_JSON> > detection.ndjson
#. /watcher_to_detection.sh <WATCHER_JSON> | jq

while getopts "h" OPTION; do
    case $OPTION in
    h)
        echo "Usage:"
        echo "$0 <WATCHER_JSON>"
        exit 0
        ;;
    esac
done

INPUT_WATCHER=$1
#OUTPUT_DETECTION=$2

if [ -z ${INPUT_WATCHER} ]; 
then
    echo "WATCHER_JSON param is mandatory"
    exit 1
fi

# Extract query parameters preserving escaping
QUERY_JSON=$( jq '.input.search.request.body.query.bool.must[].query_string.query' "$INPUT_WATCHER" | sed 's%\\%\\\\%g' )
# TODO undestrand how to replace only \ and not \"

# Create range and add it to filters, if not specified use a default range for last 30 minutes
FROM=$(jq -r '.input.search.request.body.query.bool.filter[].timestamp.gte' "$INPUT_WATCHER")
TO=$(jq -r '.input.search.request.body.query.bool.filter[].timestamp.lte' "$INPUT_WATCHER")

if [[ $FROM == "null" ]]
then 
    FROM="now-30m/m"
fi

if [[ $TO == "null" ]]
then 
     TO="now"
fi

# Build Filters array, for sigma rules only a range filter is present
FILTERS_ARRAY=()
RANGE_JSON=$( jq . templates/range.json | sed s%"_SED_TO_SED_"%"$TO"%g | sed s%"_SED_FROM_SED_"%"$FROM"%g )
FILTERS_ARRAY+=("$RANGE_JSON")

FILTERS=$( jq -s -c . <<< "${FILTERS_ARRAY[@]}" )

# Extract indices 
INDICES=$(jq -c '.input.search.request.indices ' "$INPUT_WATCHER")

# Create detection
CURRENT_DATE=$(date +"%Y-%m-%dT%H:%M:%S.%3NZ" -u)
DESCRIPTION=$(jq -r '.metadata.description' "$INPUT_WATCHER")
RULE_NAME=$(jq -r '.metadata.title' "$INPUT_WATCHER")
INTERVAL=$(jq -r '.trigger.schedule.interval' "$INPUT_WATCHER")
TAGS_ARRAY=$(jq -r -c '.metadata.tags' "$INPUT_WATCHER")
if [[ "$TAGS_ARRAY" == "" ]]; then TAGS_ARRAY="[]"; fi
RULE_ID=$(uuidgen --md5 -n @oid -N "$INPUT_WATCHER")
ID=$(uuidgen --sha1 -n @oid -N "$INPUT_WATCHER")

cat templates/detection.json \
| sed s%"_SED_CREATION_DATE_SED_"%"${CURRENT_DATE}"%g \
| sed s%"_SED_UPDATE_DATE_SED_"%"${CURRENT_DATE}"%g \
| sed s%"_SED_USER_SED_"%"${USER}"%g \
| sed s%"_SED_DESCRIPTION_SED_"%"${DESCRIPTION}"%g \
| sed s%"_SED_FROM_SED_"%"${FROM}"%g \
| sed s%"_SED_INDICES_STRING_ARRAY_SED_"%"${INDICES}"%g \
| sed s%"_SED_INTERVAL_SED_"%"$INTERVAL"%g \
| sed s%"_SED_RULE_NAME_SED_"%"${RULE_NAME}"%g \
| sed s%"_SED_TO_SED_"%"${TO}"%g \
| sed s%"_SED_FILTERS_SED_"%"${FILTERS}"%g \
| sed s%"_SED_TAGS_ARRAY_SED_"%"${TAGS_ARRAY}"%g \
| sed s%"_SED_RULE_ID_SED_"%"${RULE_ID}"%g \
| sed s%"_SED_ID_SED_"%"${ID}"%g \
| sed s@"_SED_QUERY_SED_"@"${QUERY_JSON}"@g \
| jq -c .