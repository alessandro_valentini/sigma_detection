#! /bin/bash

INPUT_WATCHER=$1
OUTPUT_DETECTION=$2

# Extract query parameters from filter and build filters array for 
FILTERS_ARRAY=()

KEYS=()
while IFS='' read -r line; do
   KEYS+=("$line")
   done < <(jq -r '.input.search.request.body.query.bool.filter[].match_phrase // [] ' "$INPUT_WATCHER" |     jq -r 'keys[]')


VALUES=()
while IFS='' read -r line; do
     VALUES+=("$line")
     done < <(jq -r '.input.search.request.body.query.bool.filter[].match_phrase // [] ' "$INPUT_WATCHER" |     jq -r 'values[]')

for ((i=0;i<${#KEYS[@]};++i)); do

   # Single Filter creation
   FILTER_JSON=$( jq . filter.json | sed s%"@@QUERY_KEY@@"%"${KEYS[i]}"%g | sed s%"@@QUERY_VALUE@@"%"${VALUES[i]}"%g )

   # concatenate json arrays
   FILTERS_ARRAY+=("$FILTER_JSON")
done

# Create range and add it to filters
FROM=$(jq -r '.input.search.request.body.query.bool.filter[].range."@timestamp".from // []' "$INPUT_WATCHER")
TO=$(jq -r '.input.search.request.body.query.bool.filter[].range."@timestamp".to // []' "$INPUT_WATCHER")
RANGE_JSON=$( jq . range.json | sed s%"@@TO@@"%"$TO"%g | sed s%"@@FROM@@"%"$FROM"%g )
FILTERS_ARRAY+=("$RANGE_JSON")

FILTERS=$( jq -s -c . <<< "${FILTERS_ARRAY[@]}" )

# Extract indices 
INDICES=$(jq -c '.input.search.request.indices ' "$INPUT_WATCHER")

# Create detection
CURRENT_DATE=$(date +"%Y-%m-%dT%H:%M:%S.%3NZ" -u)
DESCRIPTION="TODO"
RULE_NAME="TODO"

cat detection_template_new.json \
| sed s%"@@CREATION_DATE@@"%"${CURRENT_DATE}"%g \
| sed s%"@@UPDATE_DATE@@"%"${CURRENT_DATE}"%g \
| sed s%"@@USER@@"%"${USER}"%g \
| sed s%"@@DESCRIPTION@@"%"${DESCRIPTION}"%g \
| sed s%"@@FROM@@"%"${FROM}"%g \
| sed s%"@@INDICES_STRING_ARRAY@@"%"${INDICES}"%g \
| sed s%"@@INTERVAL@@"%"5m"%g \
| sed s%"@@RULE_NAME@@"%"${RULE_NAME}"%g \
| sed s%"@@TO@@"%"${TO}"%g \
| sed s%"@@FILTERS@@"%"${FILTERS}"%g \
| jq