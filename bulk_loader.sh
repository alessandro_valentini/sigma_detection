#! /bin/bash

# This script loads all ndjson file in the specified directory into kibana

# Usage
# ./bulk_loader.sh <DETECTIONS_DIR>

while getopts "h" OPTION; do
    case $OPTION in
    h)
        echo "Usage:"
        echo "$0 <DETECTIONS_DIR>"
        exit 0
        ;;
    esac
done

DETECTIONS_DIR=$1

if [ -z ${DETECTIONS_DIR} ]; 
then
    echo "DETECTIONS_DIR param is mandatory"
    exit 1
fi

DETECTIONS_DIR=$1

for DETECTION in $DETECTIONS_DIR/*.ndjson
do
    REAL_PATH=$(readlink -f $DETECTION)
    echo "Loading $REAL_PATH..."
    /usr/share/neteye/kibana/scripts/kb_curl.sh -X POST "kibana.neteyelocal:5601/api/detection_engine/rules/_import?overwrite=true" -H 'kbn-xsrf: true' -H 'Content-Type: multipart/form-data' --form "file=@$REAL_PATH"
done